#  The OpenCV webcam play.
* The project is aimed to get acquainted with openCV library.
* The experiment is shown in the ***output.avi*** file

## Usage

* Clone the repository.
* Install dependacy (see instructions [here](https://opencv.org/))
* Inside the repository folder run

```sh
python3 main.py
```

## Dependency

[OpenCV](https://opencv.org/)

## Maintainers

@MariaNema
