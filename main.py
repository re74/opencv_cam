import numpy as np
import cv2 as cv
import time

def to_gray_scale(frame):
    return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

def draw(frame, t):
    img = frame.copy()
    font = cv.FONT_HERSHEY_SIMPLEX
    img = cv.putText(img,'WoW',(int(t**2),int(t**2)), font, 4,(255,0,255),4,cv.LINE_AA)
    img = cv.line(img,(img.shape[0]//10,img.shape[1]//10),(img.shape[0]//10,img.shape[1]//20),(255,0,0),5)
    img = cv.line(img,(img.shape[0]//20,img.shape[1]//10),(img.shape[0]//20,img.shape[1]//20),(255,0,0),5)    
    pts = np.array([[img.shape[0]//10,img.shape[1]//10 + 10],[img.shape[0]//15,img.shape[1]//10 + 20], [img.shape[0]//20,img.shape[1]//10 + 10]], np.int32)
    pts = pts.reshape((-1,1,2))
    img = cv.polylines(img, [pts], False, (255, 0, 0), 5)    
    return img

def blend(frame):
    logo = cv.imread('Python-logo.png')
    logo = cv.resize(logo, (frame.shape[1], frame.shape[0]))
    dst = cv.addWeighted(frame, 0.6, logo , 0.4 , 0)
    return dst

def shuffle(frame):
    img=np.zeros((frame.shape[0], frame.shape[1],3), np.uint8)
    img[:frame.shape[0]//2,:frame.shape[1]//2] = frame[frame.shape[0]//2:,frame.shape[1]//2:]  
    img[frame.shape[0]//2:,:frame.shape[1]//2] = frame[:frame.shape[0]//2,frame.shape[1]//2:]
    img[:frame.shape[0]//2,frame.shape[1]//2:] = frame[frame.shape[0]//2:,:frame.shape[1]//2] 
    img[frame.shape[0]//2:,frame.shape[1]//2:] = frame[:frame.shape[0]//2,:frame.shape[1]//2]
    return img

def change_color(frame):
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    lower_white = np.array([0,0,170])
    upper_white = np.array([170,100,255])
    mask = cv.inRange (hsv, lower_white, upper_white)
    red = np.full_like(frame, [0,0,255])
    masked = cv.bitwise_and(frame, frame, mask = mask)   
    red = cv.bitwise_and(red, red, mask = mask)
    return frame-masked+red


cap = cv.VideoCapture(0)
fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter('output.avi', fourcc, 20.0, (640,480))
t = time.perf_counter()
while cap.isOpened():
    ret,frame = cap.read()
    if not ret:
        print("Can't get frame")
        break
    time_diff = time.perf_counter()-t
    if time_diff < 10:
        frame_to_write = frame
    elif time_diff < 20:
        frame_to_write = to_gray_scale(frame)
    elif time_diff < 30:
        frame_to_write = draw(frame, time_diff)
    elif time_diff < 40:
        frame_to_write = blend(frame)
    elif time_diff < 50:
        frame_to_write = shuffle(frame)
    elif time_diff < 60:
        frame_to_write = change_color(frame)
    else:
        break

    out.write(frame_to_write)
    cv.imshow('frame', cv.resize(frame_to_write, (320,240)))
    if cv.waitKey(1) == ord('q'):
        break
    
cap.release()
out.release()
cv.destroyAllWindows()